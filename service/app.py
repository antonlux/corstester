from flask import Flask, Response, request, jsonify
from flask.ext import restful
from flask.ext.restful.utils import cors
import json

app = Flask(__name__)
api = restful.Api(app)


class TestApi(restful.Resource):
    decorators = [cors.crossdomain(origin='*')]

    def options (self):
        return {'Allow': 'GET'}, 200, {'Access-Control-Allow-Origin': '*','Access-Control-Allow-Methods': 'GET'}

    def get(self):
        result = {
            'items':[
                {
                    'id':1,
                    'name':'Shoes'
                },
                {
                    'id': 2,
                    'name':'Fashion'
                },
                {
                    'id':3,
                    'name':'Watches'
                }
            ]
        }
        return json.dumps(result), 200, {'Content-Type': 'application/json'}

@app.route('/')
def simple_endpoint():
    result = {'message':'Hello World'}
    return json.dumps(result), 200, {'Content-Type': 'application/json'}


api.add_resource(TestApi, '/api')
if __name__ == '__main__':
    app.run(port=6001, host='0.0.0.0')






