(function(angular,$){

    var handlersModule = angular.module('TestApp',[], function ($interpolateProvider){
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    });

    handlersModule.factory('apiSvc', ['$http', function($http){
        return {
            get: function(id) {
                return $http.get('http://ec2-54-254-9-102.ap-southeast-1.compute.amazonaws.com:6001/api');
            }
        }
    }]);

    handlersModule.controller('ApiCtrl', [
        '$scope','apiSvc', function($scope, apiSvc){
            apiSvc.get()
                .success(function(data){
                    $scope.categories = data.items;
                })
        }
    ]);

})(angular, jQuery);
