from flask import Flask, Response, request, jsonify, render_template
from flask.ext.assets import Environment, Bundle

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    app.run(port=6002, host='0.0.0.0')